#!/bin/bash
# Configure an identity config file for SSH connections.
#
# Made with <3 | Please edit and share with others!
#
# ~TODO~
# * Convert to OOP / create functions
# * Make ident statement a list of options to go in and out of
# * Have option to generate SSH keys inside ident statement
# * Option list should be after inital config. Initial config should
#   happen first in order
# * Be sure to do a groovy dance
#
# Exit codes
# 1 = exit program
# 2 = invalid input
#
# Variables
sshconfig=$HOME/.ssh/config

# Check if config file exists. If not, ask to create
if [ ! -f $sshconfig ]; then
  read -p "SSH config file not found. Create one? [Y/n] " confanswer
  confanswer=${confanswer:-Y}
  if [[ "$confanswer" == "Y" || "$confanswer" == "y" ]]; then
	touch $sshconfig
	echo "File $sshconfig created."
  elif [ "$confanswer" == "n" ]; then
	echo "File $sshconfig not created."
	exit 1
  else
	echo "Invalid input."
	exit 2
  fi
else
  echo "Existing file $sshconfig found."
fi

# If file exists, check to see if it's empty
if [ ! -s $sshconfig ]
  then
	echo "Config file empty!"
fi

read -p "Would you like to configure a new connection identity? [Y/n] " identanswer
identanswer=${identanswer:-Y}

if [[ "$identanswer" == "Y" || "$identanswer" == "y" ]]; then
  PS3='Please configure the following: '
  options=("Host" "HostName" "IdentityFile" "User" "Quit")
  select opt in "${options[@]}"
  do
    case $opt in
      "Host")
		read -p "(Example: hello hello.world): " host
		echo $host
		;;
	  "HostName")
	    read -p "(Example: hello.world): " hostname
        echo $hostname
        ;;
	  "IdentityFile")
        read -p "(Example: ~/.ssh/hello.world): " identityfile
        echo $identityfile
        ;;
      "User")
        read -p "(Example: root): " user
        echo $user
        ;;
      "Quit")
        break
        ;;
	  *) echo "invalid option $REPLY";;
	esac
  done
elif [ "$identanswer" == "n" ]; then
  echo "Connection identity not created."
  exit 1
else
  echo "Invalid input."
  exit 2
fi

# If file exists, configure information
if [ -f $sshconfig ]; then
cat <<EOF >> $sshconfig
# $hostname
Host $host
  HostName $hostname
  IdentityFile $identityfile
  User $user

EOF
fi

